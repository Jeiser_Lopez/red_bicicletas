var Bicicleta = require ('../../Models/bicicletaModel');
var request = require ('request');
var server = require ('../../bin/www'); //Para que se levante el server automatico
var mongoose = require('mongoose');
const { db } = require('../../Models/bicicletaModel');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicletas API', () => {
    beforeEach(function (done) {
        var mongoDB = '/mongoDB://localhost/testDB';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const bd = mongoose.connection;
        db.on('error', console.error.bind(console, 'error de conexion'));
        db.once('open', function() {
            console.log('we are connect to test database');
            done();
        });
    });


afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success){
        if (err) console.log(err);
        done();
    });
});


/*describe('Bicicleta API', () => {
    describe('Get Bicicletas /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var bici1 = new Bicicleta(1,"Verde", "Carrera", [4.7220546,-74.2460218]);
            Bicicleta.add(bici1);

            request.get('http://localhost:3000/api/bicicletasRoutesApi', function (error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});*/

});

describe('Get bicicletas /', () => {
    it("Estatus 200", (done) => {
        request.get(base_url, function(err, response, body){
            var result = JSON.parse(body);
            expect(response.statusCode).toBe(200);
            expect(result.Bicicleta.length).toBe(0);
            done();
        });
    });
});

describe('POST BICICLETA /create', () => {
    it('STATUS 200', (done) => {
        var headers = {'content-type' : 'application/json'};
        var bici1 = '{"code" :  10 , "color" : "Rojo", "Modelo" : "Carrera", "lat" : -43, "long" : 34}';
        request.post({
            headers: headers,
            url: base_url + '/create',
            body: bici1
        }, function (error, response, body){
            expect(reponse.statusCode).toBe(200);
            //nuevo mongoose
            var bici = JSON.parse(body).Bicicleta;
            console.log(bici);
            expect(bici.color).toBe("Rojo");
            expect(bici.ubicacion[0]).toBe(-43);
            expect(bici.ubicacion[1]).toBe(34);
            done();//finaliza el test
        });
    });
});

