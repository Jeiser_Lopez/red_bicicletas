const { ExpectationFailed } = require('http-errors');
var mongoose = require('mongoose');
var Bicicleta = require('../../Models/bicicletaModel');

//mongoose
var originalTimeout;

describe('Testing Bicicletas', function(){
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection Error'));
        db.once('open', function(){
            console.log('Estamos conectados a la prueba de bd');
            done();
        });

//        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
//       jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, sucess){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [-54.4, -54.11]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toEqual(-54.4);
            expect(bici.ubicacion[1]).toEqual(-54.11);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza Vacia', (done) => {
            Bicicleta.allBicis(function(err,bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo 1 bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            Bicicleta.add(aBici, function(err, newBici ){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Devuelve bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bici) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana" });
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);

                    var abici1 = new Bicicleta({code: 2, color: "Roja", modelo: "Carrera" });
                    Bicicleta.add(aBici1, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                });
            });
        });
    });
});

/*
//Se ejecuta siemore antes de cualquier test
beforeEach(() => {
    Bicicleta.allBicis = [];
});

//Testea que el arreglo de bicicletas esta vacio
describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

//Testea que add bicicletas funcione
describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0); //Cheque a estado previo

        //Se ingresa una bici
        var bici1 = new Bicicleta(1,"Verde", "Carrera", [4.7220546,-74.2460218]);
        Bicicleta.add(bici1);

        //Se valida que se haya ingresado una bici
        expect(Bicicleta.allBicis.length).toBe(1);

        //Se valida que la bici en el arreglo sea la que se acaba de ingresar
        expect(Bicicleta.allBicis[0]).toBe(bici1);
    });
});

describe('Bicicleta.findById', () => {
    it('Devuelve una bici por id', () => {
        expect(Bicicleta.allBicis.length).toBe(0); //Cheque a estado previo
        //Se ingresa una bici
        var bici1 = new Bicicleta(1,"Verde", "Carrera", [4.7220546,-74.2460218]);
        var bici2 = new Bicicleta(2,"negro", "Montaña", [4.7220546,-74.2460218]);
        Bicicleta.add(bici1);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(bici1.color);
        expect(targetBici.modelo).toBe(bici1.modelo);
    });
});
*/
