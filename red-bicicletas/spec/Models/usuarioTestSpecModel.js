var mongoose = require ('mongoose');
var Bicicleta = require ('../../Models/bicicletaModel');
var Usuario = require ('../../Models/usuariosModel');
var Reserva = require ('../../Models/reservaModel');

describe('Testing Usuarios', function () {
    beforeEach(function(done) {
        var mongoDB = 'mongpDb/localhost/testUsu';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error de Conexion'));
        db.once('open', function() {
            console.log('Se establecio la conexion');

            done();
        });
    });

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });

    });
});

describe('Cuando se reserva una bici', () => {
    it('Debe existir la reserva', (done) => {
        const usuario = new Usuario ({ nombre: 'Jeiser' });
        usuario.save();
        const bici = new Bicicleta ({ code: 1, color: 'Negro', modelo: 'Montaña'});
        bici.save();

        var hoy = new Date();
        var maniana = new Date();
        maniana.setDate(hoy.getDate() + 1);
        usuario.Reserva(bici.id, hoy, maniana, function(err,  reserva){
            Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas){
                console.log(reservas[0]);
                expects(reservas.length).toBe(1);
                expects(reservas[0].diasDeReserva()).toBe(2);
                expects(reservas[0].bici.code).toBe(1);
                expects(reservas[0].usuario.nombre).toBe(usuario.nombre);

                done();
            })
        });
    });
});