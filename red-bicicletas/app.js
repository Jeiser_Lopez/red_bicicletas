require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var console = require('console');

var indexRouter = require('./routes/index');
var userRouter = require('./routes/userRoute');
var tokenRouter = require('./routes/tokenRoute');
var bicicletasRouter = require('./routes/bicicletasRoute');
var bicicletasApiRouter = require('./routes/api/bicicletasRoutesApi');
var usuarioApiRouter = require ('./routes/api/userRoutesApi');
var authApiRouter = require('./routes/api/authRoutesApi');

var mongoose = require('mongoose');

const passport = require('./config/passport');
const session = require('express-session');
const ReservaModel = require('./Models/reservaModel');

const jwt = require('jsonwebtoken');

const Usuario = require('./Models/usuariosModel');//
const Token = require('./Models/tokenModel');

const { token } = require('morgan');
const { compare } = require('bcrypt');
const authControllerApi = require('./controllers/api/authControllerApi');
const { assert } = require('console');

const mongoDBStore = require('connect-mongodb-session')(session);

var app = express();

let store;

if (process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
}
else{
  store = new mongoDBStore ({
    url: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error',function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}
 
app.set('secretKey', 'jwt_pwd_!!223344');
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000}, //tiempo en milisegundos
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicicletas..--22623781'
}));



//conexion en la nube
//var mongoDB = 'mongodb+srv://admin:XQirFy7Ud8W4anQh@red-bicicletas.sd1cm.mongodb.net/test?retryWrites=true&w=majority';
//conexion local 
//var mongoDB = 'mongodb://localhost/red_bicicletas';
var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB,{ useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/privacy-policy', function(req, res){
  res.sendFile('/public/privacy-policy.html');
});


app.use('/google9ec28c3e0f205dac', function(req, res){
  res.sendFile('/public/google9ec28c3e0f205dac.html');
});


app.get('/login', function(req, res){
  res.render('session/login')
});

app.post('/login', function(req, res, next){
  //passport
  passport.authenticate('local', function(err, user, info){
    console.log(user);
    if (err)
      return next(err);
    
    if (!user)
      return res.render('session/login', { info } );

    req.logIn(user, function(err){
      if (err)
        return next (err);
      res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  Usuario.findOne({ email: req.body.email }, function(err, usuario){
    if (!usuario) return res.render('session/fortgotPassword', { info: { message: 'No Existe el usuario' } } );
    usuario.resetPassword(function(err) {
      if(err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function(req, res, next){
  Token.findOne({ token: req.params.token }, function(err, token){
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No exixte el token' });

    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({ msg: 'No existe el usuario asociado a este token' });
      res.render('session/resetPassword', { errors: {}, usuario: usuario});
    });
  });

});

app.post('/resetPassword', function(req, res){
  if(req.body.password != req.body.confirm_password)
  {
    res.render('session/resetPassword', { errors: { confirm_password: { message: 'Los password no coinciden '} } } );
    return;
  }
  Usuario.findOne({ email: req.body.email }, function(err, usuario){
    usuario.password = req.body.password;
    usuario.save(function(err){
      if(err) {
        res.render('session/resetPassword', { errors: err.errors, usuario: new Usuario});
      }
      else{
        res.redirect('/login');
      }
    });
  });
});

app.get('/auth/google', 
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read' ] } )
);

app.get('/auth/google/callback',
    passport.authenticate('google', { 
     // successRedirect: '/',
     // failureRedirect: '/error'
     failureRedirect: '/login'
    }),
    function (req, res){
      res.redirect('/');
    }
);



app.use('/', indexRouter);
app.use('/usuarios', userRouter);
app.use('/token', tokenRouter);

app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletasRoutesApi', validarUsuario, bicicletasApiRouter);
app.use('/api/userRoutesApi', usuarioApiRouter);
app.use('/api/bicicletasRoutesApi', bicicletasApiRouter);
app.use('/api/userRoutesApi', usuarioApiRouter);

app.use('/api/authApiRouter', authApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if (req.user){
    next();
  }
  else{
    console.log('user sin logearse');
    res.redirect('/login');
  }
}; 

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err) res.json({ status: "error", message: err.message, data: null });
    else{
      req.body.userId = decoded.id;
      console.log('jwt: verify: '+ decoded);
      next();
    }
  });
}

module.exports = app;
