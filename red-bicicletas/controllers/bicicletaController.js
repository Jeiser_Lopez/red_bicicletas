var Bicicleta = require('../Models/bicicletaModel');

exports.bicicleta_list = function(req, res){
    res.render('bicicletaView/index', {bicis: Bicicleta.allBicis});
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletaView/create');
}

exports.bicicleta_create_post = function (req,res) {
    var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}


exports.bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.code);
    const newLocal = 'bicicletaView/update';
    res.render(newLocal, {bici});
}

exports.bicicleta_update_post = function (req,res) {
    var bici = Bicicleta.findById(req.body.code);
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.long];

    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req,res){
    Bicicleta.remove(req.body.code);

    res.redirect('/bicicletas');
}