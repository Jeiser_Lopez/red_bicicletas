var Usuario = require ('../Models/usuariosModel');

module.exports = {

    list: function(req, res, next){
        Usuario.find({}, function (err, usuarios) {
            res.render('userView/index', {usuarios: usuarios});
            //res.render('usuarios/index', {usuario: usuarios});
        });
    },

    update_get: function(req, res, next){
        Usuario.findById(req.params.id, function(err, usuario){
            res.render('userView/update', {errors: {}, usuario: usuario});
        });
    },

    update: function(req, res, next){
        var update_value = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_value, function(err, usuario){
            if (err){
                console.log(err);
                res.render('userView/update', { errors: err.errors, usuario: new Usuario({ nombre: req.body.nomnbre, email: req.body.email})});
            }
            else{
                res.redirect('usuarios');
                return;
            }
        });
    },

    create_get: function(req, res, next){
        res.render('userView/create', { errors: {}, usuario: new Usuario() });
    },

    create: function(req, res, next){
        if(req.body.password != req.body.confirm_password){
            res.render('userView/create', { errors: { confirm_password: {message: "No coninciden los password"}}, usuario: new Usuario({ nombre: req.body.nomnbre, email: req.body.email }) });
            return;
        }

        Usuario.create({ nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsuario){
            if(err){
                res.render('userView/create', { errors: {}, usuario: new Usuario({ nombre: req.body.nomnbre, email: req.body.email }) });
            }
            else{
                nuevoUsuario.enviar_email_bienvenida();
                res.redirect('/usuarios');
            }
        });
    },

    delete: function(req, res, next){
        Usuario.findByIdAndDelete(req.body.id, function(err){
            if(err){
                next(err);
            }
            else{
                res.redirect('/usuarios');
            }
        });
    }
}