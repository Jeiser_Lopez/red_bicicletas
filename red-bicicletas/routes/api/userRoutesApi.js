var express = require ('express');
var router = express.Router();
var usuarioController = require('../../controllers/api/usuarioControllersApi');

router.get('/', usuarioController.usuario_list);
router.get('/', usuarioController.usuario_create);
router.get('/', usuarioController.usuario_reservar);

module.exports = router;