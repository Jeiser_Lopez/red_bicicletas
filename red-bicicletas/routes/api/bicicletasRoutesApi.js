var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletasControllerApi');

router.get('/', bicicletaController.bicicleta_list);
router.post('/', bicicletaController.bicicleta_create);
router.delete('/', bicicletaController.bicicleta_delete);
//router.update('/', bicicletaController.bicicleta_update);

module.exports = router;