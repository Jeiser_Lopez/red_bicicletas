var mymap = L.map('main_map').setView([4.7207555,-74.253135], 13);
//4.7207555,-74.253135
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletasRoutesApi",
    success: function (result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion).addTo(mymap)
                .bindPopup('Soy de color ' + bici.color + ' y modelo ' + bici.modelo)
                .openPopup();
        });
    }
})
