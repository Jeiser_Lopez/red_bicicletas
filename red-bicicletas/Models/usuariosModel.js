var mongoose = require ('mongoose');
const { compile } = require('pug');
var Reserva = require('./reservaModel');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var mailer = require('../Mailer/mailer');

const uniqueValidator = require('mongoose-unique-validator'); // se debe agregar como plugings

const bcrypt = require ('bcrypt');
const { getMaxListeners } = require('./reservaModel');
const { stringify } = require('querystring');
const saltRound = 10;

const validateEmail = function (email){
    const re = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/;//regex
    return re.test(email);
};

var userSchema = new Schema ( {
    nombre: {
        type: String,
        trim: true, //elimina los espacios al inicio y al final
        required: [true, 'El Nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true, //elimina los espacios al inicio y al final
        required: [true, 'El Email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/]
    },
    password: {
        type: String,
        trim: true,
        required: [true, 'El Password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },

    googleId: String,
    facebookId: String

});



userSchema.plugin(uniqueValidator, {message: "El (PATH) ya existe en otro usuario"});

userSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRound);
    }
    next();
});

userSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

userSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva ({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}

userSchema.enviar_email_bienvenida = function(cb){
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this. email;
    token.save(function(err){
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de Cuenta',
            text: 'Hola \n\n' + 'Por favor hacer click en el siguiente enlace para verificar su cuenta \n' +
                    'http://localhost:3000' + '\/token/confirmation\/' + token.token + '\n' 
        };

        mailer.sendMail(mailOptions, function (err){
            if (err) { return console.log(err.message); }

            console.log('La verifiacion del email a sido enviada a : ' + email_destination);
        });
    }); 

    userSchema.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
        const self = this;
        console.log(condition);
        self.findOne({
            $or: [
                { 'googleId': condition.id }, { 'email': condition.emails[0].value}
            ]},
            (err, result) => {
                if(result){
                    callback(err, result);
                }
                else{
                    console.log('------------condition----------');
                    console.log(condition);
                    let values = {};
                    values.googleId = condition.id;
                    values.email = condition.emails[0].value;
                    values.nombre = condition.displayName || 'Sin Nombre';
                    values.verificado = true;
                    values.password = condition._json.etag;
                    console.log('------------values----------');
                    self.create(values, (err, result) => {
                        if (err) { console.log(err) };
                        return callback(err, result);
                    });
                }
            }
        )
    }

    userSchema.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
        const self = this;
        console.log(condition);
        self.findOne({
            $or: [
                { 'facebookId': condition.id }, { 'email': condition.emails[0].value}
            ]},
            (err, result) => {
                if(result){
                    callback(err, result);
                }
                else{
                    console.log('------------condition----------');
                    console.log(condition);
                    let values = {};
                    values.facebookId = condition.id;
                    values.email = condition.emails[0].value;
                    values.nombre = condition.displayName || 'Sin Nombre';
                    values.verificado = true;
                    values.password = crypto.randomBytes(16).toString('hex');
                    console.log('------------values----------');
                    self.create(values, (error, resulta) => {
                        if (error) console.log(error);
                        return callback(error, resulta);
                    });
                }
            }
        )
    }
}

module.exports = mongoose.model('Usuario', userSchema);
