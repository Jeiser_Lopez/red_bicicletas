const { NetworkAuthenticationRequire } = require("http-errors");

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var bicicletaSchema = new Schema ({
    code: Number, //No se ussa id por que es reservada en mongo
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true} //Dato de tipo geografico
    }
});

//Crear instancia
bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + '| color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb); //El primer parametro es vacio por que no hay filtro
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb); //cd callback
};

bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({ code: aCode }, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

//module.exports = Bicicleta;

/*var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return "id: " +this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBicis){
    Bicicleta.allBicis.push(aBicis);
}

Bicicleta.findById = function (aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici){
        return aBici;
    }
    else{
        throw new Error(`No existe una bicicleta con el ID  ${aBiciId}`);
    }
}

Bicicleta.remove = function (aBiciId) {
    var aBici = this.findById(aBiciId);
    console.log('hola' + aBiciId);
    for(let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
           // confirm ('Bicicleta Eliminado');
            break;
        }
        else{
            throw Error('No se encontro la bibicleta');
        }
    }
}*/

/*var a = new Bicicleta(1, 'Rojo', 'Urbana', [4.7220546,-74.2460218]);
var b = new Bicicleta(2, 'Azul', 'Urbana', [4.7236094,-74.2507653]);

Bicicleta.add(a);
Bicicleta.add(b);*/

